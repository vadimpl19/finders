object mainf: Tmainf
  Left = 487
  Top = 63
  Width = 1039
  Height = 743
  Caption = 'mainf'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object msg: TRichEdit
    Left = 0
    Top = 531
    Width = 1031
    Height = 185
    Align = alBottom
    Lines.Strings = (
      '')
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object Chart1: TChart
    Left = 0
    Top = 49
    Width = 1031
    Height = 482
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      'TChart')
    View3D = False
    Align = alClient
    TabOrder = 1
    object Series1: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      Title = 'RI'
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object Series2: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clGreen
      Title = 'SI'
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1031
    Height = 49
    Align = alTop
    Caption = 'Panel1'
    TabOrder = 2
    DesignSize = (
      1031
      49)
    object cstatel: TLabel
      Left = 8
      Top = 0
      Width = 31
      Height = 13
      Caption = 'cstatel'
    end
    object commentl: TLabel
      Left = 8
      Top = 24
      Width = 45
      Height = 13
      Caption = 'commentl'
    end
    object Button1: TButton
      Left = 664
      Top = 8
      Width = 145
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Button1'
      TabOrder = 0
      OnClick = Button1Click
    end
    object stopb: TBitBtn
      Left = 830
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1057#1090#1086#1087
      TabOrder = 1
      OnClick = stopbClick
    end
    object playb: TBitBtn
      Left = 926
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1055#1091#1089#1082
      TabOrder = 2
      OnClick = playbClick
    end
  end
end
