//---------------------------------------------------------------------------

#ifndef VECTORSH
#define VECTORSH
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#define ast AnsiString
//#include <Forms.hpp>
class tvector{
public:
  AnsiString name;
  int Count;
  double *Items;
  tvector::tvector(const AnsiString& aname,int size);
};


int loadvectors(const ast& fname,const ast& format,TList** lists);
tvector* performvector(TList *l,int shift);

double deviation(tvector* vec1,tvector* vec2,int shift,int maxcnt);
//---------------------------------------------------------------------------
#endif
