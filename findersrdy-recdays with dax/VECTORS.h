//---------------------------------------------------------------------------

#ifndef VECTORSH
#define VECTORSH
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#define ast AnsiString
//#include <Forms.hpp>
class tvector{
public:
  AnsiString name;
  int Count;
  double *Items;
  tvector::tvector(const AnsiString& aname,int size);
};


int loadvectors(const ast& fname,const ast& format,TList** lists);
tvector* performvector(TList *l,int shift);
tvector* performvectord(TList *l,int mini,int maxi);
tvector* findminmax(TList *l,int * mini,int *maxi,TDateTime *inidata,TDateTime *enddata);
void performdatatime(TList *l); //�������������� ����� ���� ������� � ������� �������
double deviation(tvector* vec1,tvector* vec2,int shift,int maxcnt);
//---------------------------------------------------------------------------
#endif
