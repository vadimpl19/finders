//---------------------------------------------------------------------------


#pragma hdrstop
#include "optimiz.h"
void alert(AnsiString s);
void addmsg(const ast& s);
void showcstate(const ast& s);
TList* params=NULL;
int stopreq=0;

void registerparamd(ast aname,double min,double max,double minopt,double maxopt,double astep,double* valp){
 if(params==NULL)params=new TList();
 tparam* par=new tparam();
 par->name=aname;
 par->pt=doublet;
 par->max=max;
 par->min=min;
 par->maxopt=maxopt;
 par->minopt=minopt;
 par->step=astep;
 par->valp=valp;
 params->Add(par);

}
void registerparami(ast aname,int amaxi,int amini,int maxopti,int minopti,int stepi,int* valpi){
if(params==NULL)params=new TList();
 tparam* par=new tparam();
 par->name=aname;
 par->pt=intt;
 par->maxi=amaxi;
 par->mini=amini;
 par->maxopti=maxopti;
 par->minopti=minopti;
 par->stepi=stepi;
 par->valpi=valpi;
 params->Add(par);

}
//---------------------------------------------------------------------------
int addstep(int i){
   tparam *par=(tparam *)params->Items[i];
   if(par->pt==doublet){
      *par->valp+=par->step;
      if(*par->valp>par->maxopt){
          *par->valp=par->minopt;
          if(params->Count<=(i+1))return -1;
          if(addstep(i+1)<0)return -1;
      }
//      par->cval=*par->valp;
   }else{
      *par->valpi=*par->valpi+par->stepi;
      if(*par->valpi>par->maxopti){
          *par->valpi=par->minopti;
          if(params->Count>=(i+1))return -1;
          if(addstep(i+1)<0)return -1;
      }
//      par->cvali=*par->valpi;

   }
   return 0;
}

void __fastcall savepar(){
 for(int i=0;i<params->Count;i++){
     tparam * par=(tparam *)params->Items[i];
     if(par->pt==doublet)
            par->cval=*par->valp;
     else par->cvali=*par->valpi;
 }
}
void __fastcall loadpar(){
 for(int i=0;i<params->Count;i++){
     tparam * par=(tparam *)params->Items[i];
     if(par->pt==doublet)
            *par->valp=par->cval;
     else *par->valpi=par->cvali;
 }
}
ast makeparlistbest(){
ast s="";
 for(int i=0;i<params->Count;i++){
     tparam * par=(tparam *)params->Items[i];
     s+=par->name+":";
     if(par->pt==doublet)
            s+=par->cval;
     else s+=par->cvali;
     s+=" ";
  }
  return s;
}
ast makeparlist(){
ast s="";
 for(int i=0;i<params->Count;i++){
     tparam * par=(tparam *)params->Items[i];
     s+=par->name+":";
     if(par->pt==doublet)
            s+=FloatToStrF(*par->valp,ffFixed,12,8);
     else s+=*par->valpi;
     s+=" ";
  }
  return s;
}
void showgraph();//���������� � mainu.cpp
void perebor(int rangechf(),double bodyf()){
tparam * par;
TList* bestpar=new TList();
double res;
bool first=true;
double maxres;
   for(int i=0;i<params->Count;i++){
       par=(tparam *)params->Items[i];
       if(par->pt==doublet)
           *par->valp=par->minopt;
        else   *par->valpi=par->minopti;
   }
   int cnt=0;
   while(addstep(0)>=0){
     if(rangechf()>=0){
         cnt++;
         Application->ProcessMessages();
         if(stopreq==2){
           addmsg("�����. ������� ������ ���.");
           addmsg(makeparlistbest()+"res="+maxres+ "1/res"+1/maxres);
         }
         while(stopreq==2)   Application->ProcessMessages();
         if(stopreq==1){
           addmsg("���������� �� �������");
           goto endl;
         }
         res=bodyf();
         if(first){
            maxres=res;
            savepar();
         }
         else if(res>maxres){
           maxres=res;
           savepar();
         }
         first=false;
         ast s=makeparlist();
         s+=" ���="+FloatToStrF(res,ffFixed,8,7)+ "      "+FloatToStrF(1/res,ffFixed,8,1);
//         addmsg(s);
        showcstate(s);
        if(res==maxres){
           addmsg(s);
           showgraph();
        }
     }
   }
endl:
   addmsg("������� ��������, ������ ���������");
   addmsg(makeparlistbest()+"res="+maxres+ "1/res"+1/maxres);
   loadpar();
   res=bodyf();

}
#pragma package(smart_init)
