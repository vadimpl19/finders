//---------------------------------------------------------------------------

#ifndef mainuH
#define mainuH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Buttons.hpp>
#include <Chart.hpp>
#include <ExtCtrls.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
//---------------------------------------------------------------------------
class Tmainf : public TForm
{
__published:	// IDE-managed Components
        TButton *Button1;
        TRichEdit *msg;
        TBitBtn *playb;
        TBitBtn *stopb;
        TLabel *cstatel;
	TChart *Chart1;
	TLineSeries *Series1;
	TLineSeries *Series2;
        TLabel *commentl;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall playbClick(TObject *Sender);
        void __fastcall stopbClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall Tmainf(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tmainf *mainf;
//---------------------------------------------------------------------------

class tspline{
  private:
   double xc,yc,y2,x2,k,b;
   double alfa_rad;
   double radkv;
   double rad;
  double offs;
  double alfa_gr;
public:
   void setparam(double arad,double aoffs,double alfa);
   __property  double radr={read =rad};
   __property  double offsr={read =offs};
   __property  double alfa_grr={read =alfa_gr};
   double recountpoint(double x);

};

#endif
