//---------------------------------------------------------------------------
#define ast AnsiString
#include <vcl.h>
#pragma hdrstop

#include "mainu.h"
#include "VECTORS.h"
#include "optimiz.h"
#include <math.h>
#include "gradientu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tmainf *mainf;
tspline spline;
//---------------------------------------------------------------------------
__fastcall Tmainf::Tmainf(TComponent* Owner)
        : TForm(Owner)
{
  DecimalSeparator='.';
 spline.setparam(40,80,1);
}
void alert(AnsiString s){
      Application->MessageBox(s.c_str(), "������", MB_OK);
};
 tvector *wtib,*wtia,*wtil;
 tvector* brentb,*brenta,*brentl;
 tvector* rib,*ria,*ril;
 tvector *BR, *RIM1,*SiM1,*SP500;
 tvector *synt;
  double br_rad,br_offs,br_alfa;
 double brc,sic,spc;
//---------------------------------------------------------------------------
//#define vectorcnt   13
#define vectorcnt   14
double offset=0;
TList * vectors[vectorcnt];

double wtibc,wtiac,wtilc;
double brentbc,brentac,brentlc;
double basec;
recountoliavg(double* oilavg){
  double maxf=brentb->Count;
  for(int i=0;i<maxf;i++){
     double d=wtia->Items[i];
     *oilavg++=((wtib->Items[i])*wtibc+
               (wtia->Items[i])*wtiac+
               wtil->Items[i]*wtilc+
               brentb->Items[i]*brentbc+
               brenta->Items[i]*brentac+
               brentl->Items[i]*brentlc)*basec+offset;
  }
}

void __fastcall  recountsynt(int maxcnt){
//  double maxf=BR->Count;
  double d;
  double * dp;
  double *brp;
  double *sip;
  dp=&synt->Items[0];
  brp=&BR->Items[0];
  sip=&SiM1->Items[0];
  for(int i=0;i<maxcnt;i++){
     *dp++=brc*(*brp++)-sic*(*sip++)+offset;
  }
}
/*
void __fastcall  recountsyntsp(int maxcnt){
//  double maxf=BR->Count;
  double d;
  double * dp;
  double *brp;
  double *sip;
  double *sp500p;
  dp=&synt->Items[0];
  brp=&BR->Items[0];
  sip=&SiM1->Items[0];
  sp500p=&SP500->Items[0];
  for(int i=0;i<maxcnt;i++){
     *dp++=brc*(*brp++)-sic*(*sip++)+spc*(*sp500p++)+offset;
  }
} */

/*
void __fastcall  recountsyntsp(int maxcnt){
//  double maxf=BR->Count;
  double d;
  double * dp;
  double *brp;
  double *sip;
  double *sp500p;
  dp=&synt->Items[0];
  brp=&BR->Items[0];
  sip=&SiM1->Items[0];
  sp500p=&SP500->Items[0];
  for(int i=0;i<maxcnt;i++){
//     *dp++=brc*(*brp++)-sic*(*sip++)+spc*(*sp500p++)+offset;
        *dp++=spline.recountpoint(i/200.0)*1e6;
  }
}
*/
void __fastcall  recountsyntsp2(int maxcnt){
//  double maxf=BR->Count;
  if((spline.radr!=br_rad)||(spline.offsr!=br_offs)||(spline.alfa_grr!=br_alfa))
     spline.setparam(br_rad,br_offs,br_alfa);
  double d;
  double * dp;
  double *brp;
  double *sip;
  double *sp500p;

  dp=&synt->Items[0];
  brp=&BR->Items[0];
  sip=&SiM1->Items[0];
  sp500p=&SP500->Items[0];
  double brcorr;
  for(int i=0;i<maxcnt;i++){
//     *dp++=brc*(*brp++)-sic*(*sip++)+spc*(*sp500p++)+offset;
     brcorr=spline.recountpoint(*brp++);
     *dp++=brc*brcorr-sic*(*sip++)+spc*(*sp500p++)+offset;
  }
}

int __fastcall  chfunsynt(){return 1;};
void showcstate(const ast& s){
   mainf->cstatel->Caption=s;
}
void addmsg(const ast& s){
     mainf->msg->SelAttributes->Color=clBlack;
     mainf->msg->Lines->Add(s);


}
int chfun(){
double d=brentbc+brentac+brentlc+wtibc+wtiac+wtilc;
  if((d>1.2)||(d<0.8))return -1;
  return 1;
}
tvector * oilavg;
int maxcnt_gl;
double simple_synt(){
  //recountoliavg(oilavg->Items);
//  recountsynt(maxcnt_gl);
//  recountsyntsp(maxcnt_gl);
  recountsyntsp2(maxcnt_gl);
  return 1/(deviation(synt,RIM1,0,maxcnt_gl));
  //return 1/(deviation(oilavg,ria,0));
  //return 1/(deviation(oilavg,ril,0));
}
void __fastcall Tmainf::Button1Click(TObject *Sender)
{
 tstgrad();
 for (int i=0;i<vectorcnt;i++){
   vectors[i]=new TList();
   vectors[i]->Capacity=40000;
 }

// int res=loadvectors("in_vect.csv","ssddddddddddd",vectors);
// int res=loadvectors("in_vect.csv","ssdddddddddddd",vectors);

// int res=loadvectors("invect.csv","sddd",vectors);
 int res=loadvectors("invect.csv","sdddd",vectors);

  double *riavg;
/*
  brentb=vectors[2];
  brenta=vectors[3];
  brentl=vectors[4];
  wtib==vectors[5];
  wtia==vectors[6];
  wtil==vectors[7];
  rib==vectors[5];
  ria==vectors[6];
  ril==vectors[7];
  */
  int shift=100;
  RIM1=performvector(vectors[1],0);
  BR=performvector(vectors[2],0);
  SiM1=performvector(vectors[3],0);
  SP500=performvector(vectors[4],0);
  /*
  wtib=performvector(vectors[5],0);
  wtia=performvector(vectors[6],0);
  wtil=performvector(vectors[7],0);
//  rib=performvector(vectors[8],0);
 // ria=performvector(vectors[9],0);
 // ril=performvector(vectors[10],0);
  rib=performvector(vectors[8],shift);
  ria=performvector(vectors[9],shift);
  ril=performvector(vectors[10],shift);
  */
  brentbc=1.0/3/2;
//  registerparamd("offset",-500,500,111000,311000,1000,&offset);
  double step=0.05;
  double cbrbase=200000.0/120;
  double csibase=200000.0/28000;
  double csp500base=200000.0/1320;

  registerparamd("offset",-500,500,-88000,111000,1000,&offset);
  registerparamd("brc",0,2,cbrbase*0.2,cbrbase*1.5,cbrbase*step,&brc);
  registerparamd("br_rad",0,2,10,40,2,&br_rad);
  registerparamd("br_offs",0,2,80,120,10,&br_offs);
  registerparamd("br_alfa",0,2,1,2,1,&br_alfa);
  registerparamd("sic",0,2,csibase*0.2,csibase*1,csibase*step,&sic);
  registerparamd("sp500",0,2,45,46,1,&spc);
//  registerparamd("sp500",0,2,csp500base*0.2,csp500base*1.5,csp500base*step,&spc);
//  oilavg=new tvector("oilavg",BR->Count);

//  ril==vectors[7];
  int maxf=BR->Count;
 // oilavg =new tvector("oilavg",maxf);
//  recountoliavg(oilavg->Items);
  synt= new tvector("synt",maxf);
//  perebor(&chfunsynt,&simple_synt);
  //maxcnt_gl=16800;
  maxcnt_gl=maxf;
  for(int i=0;i<maxf;i++){
      ast * s=(ast *)vectors[0]->Items[i];
      Series1->AddXY(i,RIM1->Items[i],*s);                   
  }

  perebor(&chfunsynt,&simple_synt);
   maxcnt_gl=maxf;
   simple_synt();
  for(int i=0;i<maxf;i++){
      ast * s=(ast *)vectors[0]->Items[i];
      Series1->AddXY(i,RIM1->Items[i],*s);
      Series2->AddXY(i,synt->Items[i],"synt");
    //  Series2->AddXY(i,200000,"synt");

  }

}
//---------------------------------------------------------------------------
void __fastcall Tmainf::playbClick(TObject *Sender)
{
if(Caption=="����"){
  Caption="�����";
  stopreq=2;
}else{
  Caption="����";
  stopreq=0;
}
}
//---------------------------------------------------------------------------

void __fastcall Tmainf::stopbClick(TObject *Sender)
{
stopreq=1;
}
//---------------------------------------------------------------------------
void showgraph(){
  mainf->Series2->Clear();
 int maxf=BR->Count;
 double oldgl=maxcnt_gl;
 maxcnt_gl=maxf;
   simple_synt();
  for(int i=0;i<maxf;i++){
//      ast * s=(ast *)vectors[0]->Items[i];

      mainf->Series2->AddXY(i,synt->Items[i],"synt");
    //  Series2->AddXY(i,200000,"synt");

  }
  maxcnt_gl=oldgl;
}
const double cos45=0.707106781;

void tspline::setparam(double arad,double aoffs,double aalfa){
  rad=arad;
  offs=aoffs;
  alfa_gr=aalfa;
  alfa_rad=2*M_PI/360*alfa_gr;
  xc=offs+rad*cos45;
  yc=offs-rad*cos45;
  y2=yc+sin(90*2*M_PI/360-alfa_rad)*rad;
  x2=xc-cos(90*2*M_PI/360-alfa_rad)*rad;
  k=tan(alfa_rad);
  b=y2-k*x2;
  radkv=rad*rad;
};
double tspline::recountpoint(double x){
  if(x<=offs){
     return x;
  }
  if(x<x2){
      double d=x-xc;
      d=sqrt(radkv-d*d);
      if(x<xc)return yc+d;
      else return yc-d;
  }
  return k*x+b;
}

