//---------------------------------------------------------------------------

#ifndef mainuH
#define mainuH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class Tmainf : public TForm
{
__published:	// IDE-managed Components
        TButton *Button1;
        TRichEdit *msg;
        TBitBtn *playb;
        TBitBtn *stopb;
        TLabel *cstatel;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall playbClick(TObject *Sender);
        void __fastcall stopbClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall Tmainf(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tmainf *mainf;
//---------------------------------------------------------------------------



#endif
