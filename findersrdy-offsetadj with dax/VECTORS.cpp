//---------------------------------------------------------------------------


#pragma hdrstop
#include "VECTORS.h"
#include "math.h"
#include "csv.h"
#include "logs.h"
#include "mainu.h"

void alert(AnsiString s);
void addmsg(const ast& s);
bool needlog= false;
////////////////////////////////////////////////////////////////////////////////
double deviation(TList* vec1,TList* vec2,int maxcnt){
////////////////////////////////////////////////////////////////////////////////
    if(vec1->Count!=vec2->Count)return (-1);
//    int register maxf=vec1->Count;

    double register sum=0;

//    for(int i=0;i<maxf;i++){
      for(int i=0;i<maxcnt;i++){
       double register d1,d2;
       d1=*(double*)vec1->Items[i];
       d2=*(double*)vec2->Items[i];
       d1=(d1-d2)*(d1-d2);
       sum+=d1;
    }
    sum=sum/maxcnt;
    sum=sqrt(sum);
}
////////////////////////////////////////////////////////////////////////////////
double decision(tvector* vec1,tvector* vec2,int shift,int maxcnt){  //vec1- ������ ���� �������� ��������, ��  ������� ������� ������
////////////////////////////////////////////////////////////////////////////////
//    if(vec1->Count!=vec2->Count)return (-1);
    int register maxf,beginf;
    if(shift<0)beginf=0;
    else {
      beginf=shift;
      if(vec1->Count>(vec2->Count+shift))maxf=vec2->Count+shift;
      else  {
        maxf=vec1->Count-shift;
        if((maxf-beginf)>vec2->Count)maxf=vec2->Count+shift;
      }
    }
    if(maxf>maxcnt)maxf=maxcnt;
    double register sum=0;
    if(maxf==beginf)return -1;
    double delta=0;
    double register d1,v1,d2;
      for(int i=0;i<maxf;i++){
       v1=vec1->Items[i-shift];
       d1=v1-vec2->Items[i-shift];
       d2=d1/v1;
       if((d2*d1)<0){//����� �����
          sum=d1*d1;
       }else{
         if(d2>0.05)d1*=1.2;
         sum+=d1*d1;
       }

    }
    sum=sum/maxcnt;
    sum=sqrt(sum);
    return sum+0.000001;
}
////////////////////////////////////////////////////////////////////////////////
double decision2(tvector* vec1,tvector* vec2,int shift,int maxcnt){  //vec1- ������ ���� �������� ��������, ��  ������� ������� ������
////////////////////////////////////////////////////////////////////////////////
//    if(vec1->Count!=vec2->Count)return (-1);
    static double first=2*0.01;
    static double next=1*0.01;
    static double basepr=1*0.01;


    static int maxlot=2;
    static double maxlev=6*0.01; //����� ����� ������ ���� ������
    static double penalty=40.0/365/5*7/14/60/100;// ����� �� ���������� maxlev �� ����� 1 ���� ( �������� �������)
    
    int curropen=0;
    int mcurropen=0;//������ curropen
    int register maxf,beginf;
    maxf=maxcnt;
    double sum=0;
    double sum2=0;
    int sign,scurropen;
    double oldl;
    double register d1,v1,d2,md2;
    int cnt=0;
      for(int i=0;i<maxf;i++){
       v1=vec1->Items[i-shift];
       d1=v1-vec2->Items[i-shift];
       sum2+=d1;
       if(v1==0){
        alert("v1==0");
       }
      // addlog((ast)"i="+i+"d1="+d1+"v1="+v1+"\n");
       d2=d1/v1;
       if(d2>0){
         md2=d2;
         sign=1;
       }else{
         md2=-d2;
         sign=-1;
       }
       if(md2>maxlev){
          sum-=penalty;
          continue;
       }
       if (curropen==0){
         if(md2>first){
            curropen-=sign;
            mcurropen=fabs(curropen);
            scurropen=-sign;
            if(needlog){
              ast s= *(AnsiString *)vectors[0]->Items[i];
              addlog(IntToStr(cnt)+" "+s+" i="+i+" d2="+d2+" curropen="+curropen+" open 0\n");
            }

         };
       }else if(md2>first+(mcurropen)*next){
                if(mcurropen>=maxlot)continue;
            curropen+=scurropen;
            mcurropen=fabs(curropen);
            if(needlog){
              ast s= *(AnsiString *)vectors[0]->Items[i];
              addlog(IntToStr(cnt)+" "+s+" i="+i+" d2="+d2+" curropen="+curropen+" open 0\n");
            }


       }else if (md2<first-basepr+(mcurropen-1)*next){//������ �������
            if(needlog){
              ast s= *(AnsiString *)vectors[0]->Items[i];
              addlog(IntToStr(cnt)+" "+s+" i="+i+" d2="+d2+" curropen="+curropen+" close\n");
            }
            sum+=basepr;
            curropen-=scurropen;
            mcurropen=fabs(curropen);
            if(mcurropen>maxlot){
              int i=1;
            }
            cnt++;

       }
    }
    if(cnt>33000){
      int i=1;
    }
    return sum+fabs(1/sum2); //������ ������� ���� ������� offset � ����
}
////////////////////////////////////////////////////////////////////////////////
double deviation(tvector* vec1,tvector* vec2,int shift,int maxcnt){
////////////////////////////////////////////////////////////////////////////////
//    if(vec1->Count!=vec2->Count)return (-1);
    int register maxf,beginf;
    if(shift<0)beginf=0;
    else {
      beginf=shift;
      if(vec1->Count>(vec2->Count+shift))maxf=vec2->Count+shift;
      else  {
        maxf=vec1->Count-shift;
        if((maxf-beginf)>vec2->Count)maxf=vec2->Count+shift;
      }
    }
    if(maxf>maxcnt)maxf=maxcnt;
    double register sum=0;
    if(maxf==beginf)return -1;

    for(int i=beginf;i<maxf;i++){
       double register d1,d2;
       d1=vec1->Items[i-shift];
       d2=vec2->Items[i];
       d1=(d1-d2)*(d1-d2);
       sum+=d1;
    }
    sum=sum/(maxf-beginf);
    sum=sqrt(sum);
    return sum;
}
////////////////////////////////////////////////////////////////////////////////
double deviationavg(TList* vec1,TList* vec2){
////////////////////////////////////////////////////////////////////////////////
    if(vec1->Count!=vec2->Count)return (-1);
    int register maxf=vec1->Count;

    double register sum=0;

    for(int i=0;i<maxf;i++){
       double register d1,d2;
       d1=*(double *)vec1->Items[i];
       d2=*(double *)vec2->Items[i];
       d1=(d2-d1);
       sum+=d1;
    }
    sum=sum/maxf;
    sum=sqrt(sum);
}
////////////////////////////////////////////////////////////////////////////////
double avg(TList* vec1){
////////////////////////////////////////////////////////////////////////////////
    int register maxf=vec1->Count;

    double register sum=0;

    for(int i=0;i<maxf;i++){
       double register d1,d2;
       d1=*(double *)vec1->Items[i];
       sum+=d1;
    }
    sum=sum/maxf;
    sum=sqrt(sum);
}
////////////////////////////////////////////////////////////////////////////////
double avg2(TList* vec1){
////////////////////////////////////////////////////////////////////////////////
    int register maxf=vec1->Count;

    double register sum=0;

    for(int i=0;i<maxf;i++){
       double register d1,d2;
       d1=*(double *)vec1->Items[i];
       sum+=d1*d1;
    }
    sum=sum/maxf;
    sum=sqrt(sum);
}
////////////////////////////////////////////////////////////////////////////////
int loadvectors(const ast& fname,const ast& format,TList** lists){
////////////////////////////////////////////////////////////////////////////////
char * buf;
ast * sp;
double * dp;
int j;
   int h=FileOpen(fname,fmOpenRead);
   if (h==-1){alert("���� "+fname+" �� ��������");return -1;};
   int   iFileLength = FileSeek(h,0,2);
   FileSeek(h,0,0); //
   buf = new char[iFileLength+1];
   int cnt=0;
   int br = FileRead(h, buf, iFileLength);
      if (br==-1){alert("���� "+fname+" �� ��������");return -2;};
      FileClose(h);
      delim=';';

   int i=0;
   try{
   do{
      cnt++;
      for(j=0;j<format.Length();j++){
        char* ch=format.c_str();
//        switch   (format[j]){
        switch   (ch[j]){
        case 's':
        case 'S':
           sp=new AnsiString();
           getitem(sp,buf,&i,br);
           lists[j]->Add(sp);
        break;
        case 'd':
        case 'D':
           dp=new double;
           *dp=getdouble(buf,&i, br );
           lists[j]->Add(dp);
        break;
        default:
           alert("����������� ������ � ������ �������");
            delete [] buf;
            return (-1);
        }

     }
     }while(i<br);
        addmsg("�������� ����� ���������. ����������"+IntToStr(cnt)+"�����");
        delete [] buf;
        return cnt;


//   }catch(EConvertError&){
   }catch(...){
            alert("�������� ������ �����, �������� ��������");

            delete [] buf;
            return (-1);
        }



}
////////////////////////////////////////////////////////////////////////////////
tvector::tvector(const ast& aname,int size){
////////////////////////////////////////////////////////////////////////////////
  name=aname;
  Count=size;
  Items=new double[size];
}
////////////////////////////////////////////////////////////////////////////////
tvector* performvector(TList *l,int shift){
////////////////////////////////////////////////////////////////////////////////
 if(shift<0)return NULL;
 tvector* vec=new tvector("noname",l->Count-shift);
 int maxf=l->Count;
 for (int i=shift;i<maxf;i++){
   vec->Items[i-shift]=*(double*)l->Items[i];
 }
 return vec;
}

//---------------------------------------------------------------------------

#pragma package(smart_init)
