//---------------------------------------------------------------------------

#ifndef optimizH
#define optimizH
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#define ast AnsiString

enum partype { intt,doublet};
class tparam{
public:
  ast name;
  partype pt;
  double max,min;
  double maxopt,minopt,step;
  double *valp;
  double cval;//�������� �������� ��� ����������� ����������� ����, ����� ��� ����. ��������
  int maxi,mini;
  int maxopti,minopti,stepi;
  int *valpi;
  int cvali; //�������� �������� ��� ����������� ����������� ����, ����� ��� ����. ��������
};
void registerparamd(ast aname,double amax,double amin,double maxopt,double minopt,double step,double* valp);
void registerparami(ast aname,int amaxi,int amini,int maxopti,int minopti,int stepi,int* valpi);
void perebor(int rangechf(),double bodyf());
extern int stopreq;//������ �� ������� ����������� 1 - ������ �������
                   //                              2- �����
//---------------------------------------------------------------------------
#endif
