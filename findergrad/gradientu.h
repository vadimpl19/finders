//---------------------------------------------------------------------------

#ifndef gradientuH
#define gradientuH
//---------------------------------------------------------------------------
int tstgrad();// ���� ������������ ������
#endif
/*

�������� ��� ���������

������ ��������������� ����� �������� � ������������. ������ � ��� ����� ���������, ���������� � ������� � ��������� ������. ����������� ���������� ������ ���������� ������� ������� ��� ��������������� ��������� ������.

������ � ����������� ������� ����� �������� � ������� ��� ������� size() � capacity():

cout <<"Size: "<< vdouble.size ()<<"Capacity: " << vdouble .capacity () <<end1;

���� ��� ������� max_size(), ������� ���������� ������� ������ ������� � �����������, ������������ ������ �������� ����������� ���������� ����� ������.

������� resize () ��������� �������� ������ �������. ������� is_empty () ���������� true, ���� ������ ���� (������ ����� 0). ����������� ������� ����� �������� ��� �������� reserve (). ���������������� �������������� ��������� �������� ������ �������������� ��������� ������:

vdouble.reserve (1000);

������� clear () ������� ��� �������� �������.

������� assign () ����������� ��������� �������� ������ n ���������:

vdouble.assign (100, 1.0);

�������������� ����� ������� ��������� ��������� ��������� �������� �� ��������� ������� ����������:

void assign (Input-Iterator first, Inputlterator last);

������� front() � back () ���������� �������� �������������� ������� � ���������� ��������� �������.

�������� ����� ������������� ��������� �������� ��������, ��������, erase () � insert (). ��� ������ ��� �������� � ������� ���������. ��� �������� �������� (��� ���������� ���������) �� ������� ��� ����������� ���������� � ������. ��� ������� �����������; �� �������� ������ ��� ����� insert ():

iterator erase (iterator position) ;

iterator erase (iterator first, iterator last) ;

iterator insert(iterator pos, const T& x);

void insert(iterator pos,

Inputlterator first, Inputlterator last) ;

��� ������� ��������� �������������� ������� �������, ������� ��������, �������� ������� � �������� �������� ��������� �� ������� ����������.

� ������� ������� push_back () � pop_back () ������ ����� ������������ ��� ����. ������ �� ��� ��������� �������
� ����� �������, ������ � ������� �������� ������� (��� �� ���������� ��� ��������).

��� ��������� �����������:

#include <iostream>

#include <vector> 

#pragma hdrstop

#include <condefs.h>

using namespace std;

// 

// ��������������� ������� ��� ������ ����������� ����������

//

template<class T> void Display(const � &�) {

cout<< "( " ;

copy(�.begin (), c.end(),

ostream_iterator<T::value_type> (cout, " "));

cout << "} Size: " << c.size() << endl;

}

int main ()

(

int iarr[5] = (1, 2, 3, 4, 5);

vector<int> vintl(iarr, iarr + 5);

cout << "Initial vector : ";

Display(vinti);

vector<int>::iterator p =

find(vinti.begin (), vintl.end(), 3);

vinti.erase (p);

cout << "After erasing 3: ";

Display(vinti);

vinti.insert (p, iarr, iarr + 3);

cout << "After insertion: ";

Display(vinti);

cout << "Pop and push : ";

vinti.pop_back();

Display(vinti);

vinti.push back(33);

cout << " Display(vinti);

vinti.pop_back ();

return 0;

}

��������� �������:

Initial vector : {12345} Size: 5

After erasing 3: {1245} Size: 4 

After insertion: {1212345} Size: 7 

Pop and push : {121234} Size: 6

{ 1 2 1 2 3 4 33 } Size: 7

������ ������ �������-��������� ������� �� ������ ����� � ����������� ������� C++Builder'a.

*/