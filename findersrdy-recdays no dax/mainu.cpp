//---------------------------------------------------------------------------
#define ast AnsiString
#include <vcl.h>
#pragma hdrstop

#include "mainu.h"
#include "VECTORS.h"
#include "optimiz.h"
#include "logs.h"
#include <math.h>
#include <inifiles.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
int shift=0;
Tmainf *mainf;
tspline spline;
TIniFile * inil=NULL; //���- ���� ���� �����. ��������
ast datestr;
//---------------------------------------------------------------------------
__fastcall Tmainf::Tmainf(TComponent* Owner)
        : TForm(Owner)
{
  DecimalSeparator='.';
 spline.setparam(40,80,1);
}
void alert(AnsiString s){
      Application->MessageBox(s.c_str(), "������", MB_OK);
};
 tvector *wtib,*wtia,*wtil;
 tvector* brentb,*brenta,*brentl;
 tvector* rib,*ria,*ril;
 tvector *BR, *RIM1,*SiM1,*SP500;
 tvector *synt;
  double br_rad,br_offs,br_alfa;
 double brc,sic,spc;
//---------------------------------------------------------------------------
//#define vectorcnt   13
#define vectorcnt   14
//double offset=0;
extern double offset;
TList * vectors[vectorcnt];

double wtibc,wtiac,wtilc;
double brentbc,brentac,brentlc;
double basec;
recountoliavg(double* oilavg){
  double maxf=brentb->Count;
  for(int i=0;i<maxf;i++){
     double d=wtia->Items[i];
     *oilavg++=((wtib->Items[i])*wtibc+
               (wtia->Items[i])*wtiac+
               wtil->Items[i]*wtilc+
               brentb->Items[i]*brentbc+
               brenta->Items[i]*brentac+
               brentl->Items[i]*brentlc)*basec+offset;
  }
}

void __fastcall  recountsynt(int maxcnt){
//  double maxf=BR->Count;
  double d;
  double * dp;
  double *brp;
  double *sip;
  dp=&synt->Items[0];
  brp=&BR->Items[0];
  sip=&SiM1->Items[0];
  for(int i=0;i<maxcnt;i++){
     *dp++=brc*(*brp++)-sic*(*sip++)+offset;
  }
}
/*
void __fastcall  recountsyntsp(int maxcnt){
//  double maxf=BR->Count;
  double d;
  double * dp;
  double *brp;
  double *sip;
  double *sp500p;
  dp=&synt->Items[0];
  brp=&BR->Items[0];
  sip=&SiM1->Items[0];
  sp500p=&SP500->Items[0];
  for(int i=0;i<maxcnt;i++){
     *dp++=brc*(*brp++)-sic*(*sip++)+spc*(*sp500p++)+offset;
  }
} */

/*
void __fastcall  recountsyntsp(int maxcnt){
//  double maxf=BR->Count;
  double d;
  double * dp;
  double *brp;
  double *sip;
  double *sp500p;
  dp=&synt->Items[0];
  brp=&BR->Items[0];
  sip=&SiM1->Items[0];
  sp500p=&SP500->Items[0];
  for(int i=0;i<maxcnt;i++){
//     *dp++=brc*(*brp++)-sic*(*sip++)+spc*(*sp500p++)+offset;
        *dp++=spline.recountpoint(i/200.0)*1e6;
  }
}
*/
void __fastcall  recountsyntsp2(int maxcnt){
//  double maxf=BR->Count;
  if((spline.radr!=br_rad)||(spline.offsr!=br_offs)||(spline.alfa_grr!=br_alfa))
     spline.setparam(br_rad,br_offs,br_alfa);
  double d;
  double * dp;
  double *brp;
  double *sip;
  double *sp500p;

  dp=&synt->Items[0];
  brp=&BR->Items[0];
  sip=&SiM1->Items[0];
  sp500p=&SP500->Items[0];
  double brcorr;
  for(int i=0;i<maxcnt;i++){
//     *dp++=brc*(*brp++)-sic*(*sip++)+spc*(*sp500p++)+offset;
     brcorr=spline.recountpoint(*brp++);
     *dp++=brc*brcorr-sic*(*sip++)+spc*(*sp500p++)+offset;
  }
}

int __fastcall  chfunsynt(){return 1;};
void showcstate(const ast& s){
   mainf->cstatel->Caption=s;
}
void addmsg(const ast& s){
     mainf->msg->SelAttributes->Color=clBlack;
     mainf->msg->Lines->Add(s);


}
int chfun(){
double d=brentbc+brentac+brentlc+wtibc+wtiac+wtilc;
  if((d>1.2)||(d<0.8))return -1;
  return 1;
}
tvector * oilavg;
int maxcnt_gl;
double simple_synt(){
  //recountoliavg(oilavg->Items);
//  recountsynt(maxcnt_gl);
//  recountsyntsp(maxcnt_gl);
  recountsyntsp2(maxcnt_gl);
  double dev=deviation(synt,RIM1,shift,maxcnt_gl);
  if (dev!=0)return 1/dev;
  else return -1;
  //return 1/(deviation(oilavg,ria,0));
  //return 1/(deviation(oilavg,ril,0));
}
void __fastcall Tmainf::Button1Click(TObject *Sender)
{
 inilog("0offsets.csv");
 for (int i=0;i<vectorcnt;i++){
   vectors[i]=new TList();
   vectors[i]->Capacity=40000;
 }
ast s=ExtractFileDir(Application->ExeName)+"\\ini\\ini.ini";
  TIniFile *ini=new TIniFile(s);
  s=ini->ReadString("common","infile","");
int res=loadvectors(s,"sdddd",vectors);
   commentl->Caption=ini->ReadString("common","comment","");

  double *riavg;
//  TDateTimeFlag
  TDateTime* inidata=new TDateTime("28.03.11 10:01:20");
  TDateTime* enddata;
  TDateTime* curd;
  extern double offset,inistep,endoffs;

  ast inidatas=ini->ReadString("common","inidata","");
  ast enddatas=ini->ReadString("common","enddata","");
  inidata=new TDateTime(inidatas);
  enddata=new TDateTime(enddatas);
    //���� ������ � ����� �������
  int maxf,mini,maxi;

  offset=ini->ReadFloat("common","offsetstart",0);
  inistep=ini->ReadFloat("common","inistep",10000);
  endoffs=ini->ReadFloat("common","endoffs",0);
  s="sp500";
  registerparamd(s,-500,500,ini->ReadFloat(s,"begin",0),
                                   ini->ReadFloat(s,"end",0),
                                   ini->ReadFloat(s,"step",1),
                                   &spc);
  s="brc";
  registerparamd(s,-500,500,ini->ReadFloat(s,"begin",0),
                                   ini->ReadFloat(s,"end",0),
                                   ini->ReadFloat(s,"step",1),
                                   &brc);
   s="sic";
  registerparamd(s,-500,500,ini->ReadFloat(s,"begin",0),
                                   ini->ReadFloat(s,"end",0),
                                   ini->ReadFloat(s,"step",1),
                                   &sic);
  registerparamd("br_offs",0,2,130,-140,10,&br_offs);
  registerparamd("br_rad",0,2,20,-30,5,&br_rad);
 registerparamd("br_alfa",0,2,1,-45,10,&br_alfa);



  maxi=1;
  bool first=true;
  performdatatime(vectors[0]);
  findminmax(vectors[0],&mini,&maxi,inidata,enddata);
  inil=new TIniFile("ini\\followbrc.ini");
  while(maxi>0){
     ShortDateFormat="dd.mm.yy";
    datestr=enddata->DateString();
    /*
    RIM1=performvector(vectors[1],0);
    BR=performvector(vectors[2],0);
    SiM1=performvector(vectors[3],0);
    SP500=performvector(vectors[4],0);
    */
    if(RIM1!=NULL){
      delete RIM1;
      delete BR;
      delete SiM1;
      delete SP500;
    }else{                                                  
    }
    RIM1=performvectord(vectors[1],mini,maxi);
    BR=performvectord(vectors[2],mini,maxi);
    SiM1=performvectord(vectors[3],mini,maxi);
    SP500=performvectord(vectors[4],mini,maxi);
    brentbc=1.0/3/2;
    double cbrbase=200000.0/120;
    double csibase=200000.0/28000;
    double csp500base=200000.0/1320;
    double step=0.1;
    //   s=ini->ReadString("common","infile","");
    // int res=loadvectors(s,"sdddd",vectors);
    maxf=RIM1->Count;
    synt= new tvector("synt",maxf);
    maxcnt_gl=maxf;
     mainf->Series1->Clear();
       for(int i=0;i<maxf;i++){
          ast * s=(ast *)vectors[0]->Items[i];
          Series1->AddXY(i,RIM1->Items[i],*s);
       }

  perebor(&chfunsynt,&simple_synt);
   maxcnt_gl=maxf;
   simple_synt();
//   ast s=enddate->

//   ini->WriteFloat("offsets",s,offset);
   
   //alert(s+" offset="+offset);
   *enddata+=1;
   *inidata+=1;
   findminmax(vectors[0],&mini,&maxi,inidata,enddata);
 } //while
}
//---------------------------------------------------------------------------
void __fastcall Tmainf::playbClick(TObject *Sender)
{
if(Caption=="����"){
  Caption="�����";
  stopreq=2;
}else{
  Caption="����";
  stopreq=0;
}
}
//---------------------------------------------------------------------------

void __fastcall Tmainf::stopbClick(TObject *Sender)
{
stopreq=1;
}
//---------------------------------------------------------------------------
void showgraph(){
  mainf->Series2->Clear();
 int maxf=BR->Count;
 double oldgl=maxcnt_gl;
 maxcnt_gl=maxf;
   simple_synt();
  for(int i=0;i<maxf;i++){
//      ast * s=(ast *)vectors[0]->Items[i];

      mainf->Series2->AddXY(i,synt->Items[i],"synt");
    //  Series2->AddXY(i,200000,"synt");

  }
  maxcnt_gl=oldgl;
}
const double cos45=0.707106781;

void tspline::setparam(double arad,double aoffs,double aalfa){
  rad=arad;
  offs=aoffs;
  alfa_gr=aalfa;
  alfa_rad=2*M_PI/360*alfa_gr;
  xc=offs+rad*cos45;
  yc=offs-rad*cos45;
  y2=yc+sin(90*2*M_PI/360-alfa_rad)*rad;
  x2=xc-cos(90*2*M_PI/360-alfa_rad)*rad;
  k=tan(alfa_rad);
  b=y2-k*x2;
  radkv=rad*rad;
};
double tspline::recountpoint(double x){
  if(x<=offs){
     return x;
  }
  if(x<x2){
      double d=x-xc;
      d=sqrt(radkv-d*d);
      if(x<xc)return yc+d;
      else return yc-d;
  }
  return k*x+b;
}

