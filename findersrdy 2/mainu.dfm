object mainf: Tmainf
  Left = 193
  Top = 118
  Width = 928
  Height = 853
  Caption = 'mainf'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object cstatel: TLabel
    Left = 8
    Top = 0
    Width = 31
    Height = 13
    Caption = 'cstatel'
  end
  object commentl: TLabel
    Left = 8
    Top = 24
    Width = 45
    Height = 13
    Caption = 'commentl'
  end
  object Button1: TButton
    Left = 392
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object msg: TRichEdit
    Left = 0
    Top = 641
    Width = 920
    Height = 185
    Align = alBottom
    Lines.Strings = (
      '')
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object playb: TBitBtn
    Left = 624
    Top = 72
    Width = 75
    Height = 25
    Caption = #1055#1091#1089#1082
    TabOrder = 2
    OnClick = playbClick
  end
  object stopb: TBitBtn
    Left = 536
    Top = 72
    Width = 75
    Height = 25
    Caption = #1057#1090#1086#1087
    TabOrder = 3
    OnClick = stopbClick
  end
  object Chart1: TChart
    Left = 8
    Top = 120
    Width = 897
    Height = 513
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      'TChart')
    View3D = False
    TabOrder = 4
    object Series1: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      Title = 'RI'
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object Series2: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clGreen
      Title = 'SI'
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
end
