//---------------------------------------------------------------------------
#define ast AnsiString
#include <vcl.h>
#pragma hdrstop

#include "mainu.h"
#include "VECTORS.h"
#include "optimiz.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tmainf *mainf;
//---------------------------------------------------------------------------
__fastcall Tmainf::Tmainf(TComponent* Owner)
        : TForm(Owner)
{
}
void alert(AnsiString s){
      Application->MessageBox(s.c_str(), "������", MB_OK);
};
 tvector *wtib,*wtia,*wtil;
 tvector* brentb,*brenta,*brentl;
 tvector* rib,*ria,*ril;
//---------------------------------------------------------------------------
//#define vectorcnt   13
#define vectorcnt   14
double offset=0;
TList * vectors[vectorcnt];

double wtibc,wtiac,wtilc;
double brentbc,brentac,brentlc;
double basec;
recountoliavg(double* oilavg){
  double maxf=brentb->Count;
  for(int i=0;i<maxf;i++){
     double d=wtia->Items[i];
     *oilavg++=((wtib->Items[i])*wtibc+
               (wtia->Items[i])*wtiac+
               wtil->Items[i]*wtilc+
               brentb->Items[i]*brentbc+
               brenta->Items[i]*brentac+
               brentl->Items[i]*brentlc)*basec+offset;
  }
}
void showcstate(const ast& s){
   mainf->cstatel->Caption=s;
}
void addmsg(const ast& s){
     mainf->msg->SelAttributes->Color=clBlack;
     mainf->msg->Lines->Add(s);


}
int chfun(){
double d=brentbc+brentac+brentlc+wtibc+wtiac+wtilc;
  if((d>1.2)||(d<0.8))return -1;
  return 1;
}
tvector * oilavg;

double bodyftst(){
  recountoliavg(oilavg->Items);
  return 1/(deviation(oilavg,rib,0));
  //return 1/(deviation(oilavg,ria,0));
  //return 1/(deviation(oilavg,ril,0));
}
void __fastcall Tmainf::Button1Click(TObject *Sender)
{
 for (int i=0;i<vectorcnt;i++){
   vectors[i]=new TList();
   vectors[i]->Capacity=40000;
 }

// int res=loadvectors("in_vect.csv","ssddddddddddd",vectors);
 int res=loadvectors("in_vect.csv","ssdddddddddddd",vectors);

  double *riavg;
/*
  brentb=vectors[2];
  brenta=vectors[3];
  brentl=vectors[4];
  wtib==vectors[5];
  wtia==vectors[6];
  wtil==vectors[7];
  rib==vectors[5];
  ria==vectors[6];
  ril==vectors[7];
  */
  int shift=100;
  brentb=performvector(vectors[2],0);
  brenta=performvector(vectors[3],0);
  brentl=performvector(vectors[4],0);
  wtib=performvector(vectors[5],0);
  wtia=performvector(vectors[6],0);
  wtil=performvector(vectors[7],0);
//  rib=performvector(vectors[8],0);
 // ria=performvector(vectors[9],0);
 // ril=performvector(vectors[10],0);
  rib=performvector(vectors[8],shift);
  ria=performvector(vectors[9],shift);
  ril=performvector(vectors[10],shift);
  brentbc=1.0/3/2;
  registerparamd("offset",-500,500,-600,0,5,&offset);
  double step=0.1;
  registerparamd("brentbc",0,1,0,1,step,&brentbc);
  brentac=1.0/3/2;
  registerparamd("brentac",0,1,0,1,step,&brentac);
  brentlc=1.0/3/2;
  registerparamd("brentlc",0,1,0,1,step,&brentlc);
  wtibc=1.0/3/2;
  registerparamd("wtibc",0,1,0,1,step,&wtibc);
  wtiac=1.0/3/2;
  registerparamd("wtiac",0,1,0,1,step,&wtiac);
  wtilc=1.0/3/2;
  registerparamd("wtilc",0,1,0,1,step,&wtilc);

  basec=ril->Items[0]/brentl->Items[0];
  registerparami("shift",0,11,shift,shift,1,&shift);

  oilavg=new tvector("oilavg",brentb->Count);

//  ril==vectors[7];
  int maxf=wtib->Count;
  oilavg =new tvector("oilavg",maxf);
  recountoliavg(oilavg->Items);
//  for(double i=0;i)

//  double resd=deviation(oilavg,rib,0);
//  addmsg("dev="+FloatToStr(resd));
  perebor(&chfun,&bodyftst);


}
//---------------------------------------------------------------------------
void __fastcall Tmainf::playbClick(TObject *Sender)
{
if(Caption=="����"){
  Caption="�����";
  stopreq=2;
}else{
  Caption="����";
  stopreq=0;
}
}
//---------------------------------------------------------------------------

void __fastcall Tmainf::stopbClick(TObject *Sender)
{
stopreq=1;        
}
//---------------------------------------------------------------------------

