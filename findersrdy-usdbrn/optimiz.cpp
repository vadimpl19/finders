//---------------------------------------------------------------------------


#pragma hdrstop
#include "optimiz.h"
#include "vectors.h"
#include "logs.h"
void alert(AnsiString s);
void addmsg(const ast& s);
void showcstate(const ast& s);
TList* params=NULL; //������ ���������� ��� ����������� �� ���� ��������
int stopreq=0;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void registerparamd(ast aname,double min,double max,double minopt,double maxopt,double astep,double* valp){
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 if(params==NULL)params=new TList();
 tparam* par=new tparam();
 par->name=aname;
 par->pt=doublet;
 par->max=max;
 par->min=min;
 par->maxopt=maxopt;
 par->minopt=minopt;
 par->step=astep;
par->valp=valp;
//par->valp=minopt;
*par->valp=minopt;
 params->Add(par);

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void registerparami(ast aname,int amaxi,int amini,int maxopti,int minopti,int stepi,int* valpi){
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if(params==NULL)params=new TList();
 tparam* par=new tparam();
 par->name=aname;
 par->pt=intt;
 par->maxi=amaxi;
 par->mini=amini;
 par->maxopti=maxopti;
 par->minopti=minopti;
 par->stepi=stepi;
 par->valpi=valpi;
 params->Add(par);

}
//---------------------------------------------------------------------------
int addstep(int i){
   tparam *par=(tparam *)params->Items[i];
   if(par->pt==doublet){
      *par->valp+=par->step;
      if(*par->valp>par->maxopt){
          *par->valp=par->minopt;
          if(params->Count<=(i+1))return -1;
          if(addstep(i+1)<0)return -1;
      }
//      par->cval=*par->valp;
   }else{
      *par->valpi=*par->valpi+par->stepi;
      if(*par->valpi>par->maxopti){
          *par->valpi=par->minopti;
          if(params->Count>=(i+1))return -1;
          if(addstep(i+1)<0)return -1;
      }
//      par->cvali=*par->valpi;

   }
   return 0;
}
bool parsaved=false;
double lastoffs;
extern double offset;
void __fastcall savepar(){
 lastoffs=offset;
 for(int i=0;i<params->Count;i++){
     tparam * par=(tparam *)params->Items[i];
     if(par->pt==doublet)
            par->cval=*par->valp;
     else par->cvali=*par->valpi;

 }
 parsaved=true;
}
void __fastcall loadpar(){
 offset=lastoffs;
 for(int i=0;i<params->Count;i++){
     tparam * par=(tparam *)params->Items[i];
     if(par->pt==doublet)
            *par->valp=par->cval;
     else *par->valpi=par->cvali;
 }
}
ast makeparlistbest(){
ast s="";
 for(int i=0;i<params->Count;i++){
     tparam * par=(tparam *)params->Items[i];
     s+=par->name+":";
     if(par->pt==doublet)
            s+=par->cval;
     else s+=par->cvali;
     s+=" ";
  }
  return s;
}
ast makeparlist(){
ast s="";
 for(int i=0;i<params->Count;i++){
     tparam * par=(tparam *)params->Items[i];
     s+=par->name+":";
     if(par->pt==doublet)
            s+=FloatToStrF(*par->valp,ffFixed,12,8);
     else s+=*par->valpi;
     s+=" ";
  }
  return s;
}
void showgraph();//���������� � mainu.cpp
struct tpoint{
  double offs;
  double res; //���������
};
double inistep=10000;
double endoffs=1000;
//double offset=0;
//double offset=-300000;
double offset=0;
////////////////////////////////////////////////////////////
double bodyoffset(double offsa,double bodyf(),double inistep){//������� ������������ �������� ����������
// offsa - ��������� �������� offset
// bodyf() -�������  ������� ���������� ����������� ( ������ ��������. ���������� ������� � �����)
// inistep - �������� ��� ��������� offset. ���� ==0� �� ����������� �� ����������
////////////////////////////////////////////////////////////////
  tpoint risear[2];
  tpoint failar[2];
  bool rising=true;
  offset=offsa;


  double res=bodyf();
  if(inistep==0){
   return res; //����������� �� ���������, ������� � ��������� ���������
  }
  risear[1].offs=offset;
  risear[1].res=res;
  failar[0].offs=offset;
  failar[0].res=res;

  offset+=inistep;
  res=bodyf();
  if(res>risear[1].res){
    risear[1].res=res;
    risear[1].offs=offset;
    rising=true;
    nextrisel:
    offset+=inistep;
    res=bodyf();
    if(res>risear[1].res){
      risear[0]=risear[1];
      risear[1].res=res;
      risear[1].offs=offset;
      goto nextrisel;
    }
     failar[0]=risear[1];
     failar[1].res=res;
     failar[1].offs=offset;
//     goto adjpointl;
  }else{
    failar[1].res=res;
    failar[1].offs=offset;
    offset=offsa;
    rising=false;
nextfaill:
      offset-=inistep;
    res=bodyf();
    if(res>failar[0].res){
      failar[1]=failar[0];
      failar[0].res=res;
      failar[0].offs=offset;
      goto nextfaill;
    }
     risear[0].res=res;
     risear[0].offs=offset;
     risear[1]=failar[0];
  }
  //����� ������ ����������
  bool loopcont=true;
  extern bool needlog;
  needlog=false;

  while(loopcont){
    if((failar[1].offs-risear[0].offs)<endoffs){//�������� �� �������� ������� offseta. ����� �� ����������
      needlog=true;
      loopcont=false;
    }
  bool left=false;
   if((risear[1].offs-risear[0].offs)==(failar[1].offs-failar[0].offs)){
     if(risear[0].res<failar[1].res){
        //��������� �������� �� ������ ������  - ����� ������� �����
        left=true;
     }
   }else
       if((risear[1].offs-risear[0].offs)>(failar[1].offs-failar[0].offs))
           left=true; //����� ������� ������ ������� �������
   if(left){
    //��������� �������� �� ����� ������
    offset=   (risear[1].offs+risear[0].offs)/2;
    res=bodyf();
    if(res>risear[1].res){ // ��� ����� ��������.
      failar[1]=failar[0];
      risear[1].res=res;
      risear[1].offs=offset;
      failar[0]=risear[1];
    }else{        //��������� ������ �������������. ������� ����� rise[0];
      risear[0].res=res;
      risear[0].offs=offset;

    }
  }else{
      //��������� �������� �� ������ ������
    offset=   (failar[0].offs+failar[1].offs)/2;
    res=bodyf();
    if(res>failar[0].res){ // ��� ����� ��������.
       // failar[1]=failar[0]; !!!������
       risear[0]=risear[1];

      risear[1].res=res;
      risear[1].offs=offset;
      failar[0]=risear[1];
    }else{ //������� ����� failar[1]
      failar[1].res=res;
      failar[1].offs=offset;
    }

  }
 }
 needlog=false;
 offset=risear[1].offs;
 return risear[1].res;
};

double offsstep=1000;
struct tfrar{
  double offs;
  double v; //���������
};
tfrar failfr[100];//������ � ��������� ������� � ���������
tfrar risefr[100];//������ � ������������ ������� � ��������� ���������


void perebor(int rangechf(),double bodyf()){
tparam * par;
TList* bestpar=new TList();
double res;
bool first=true;
double maxres;
   for(int i=0;i<params->Count;i++){
       par=(tparam *)params->Items[i];
       if(par->pt==doublet)
           *par->valp=par->minopt;
        else   *par->valpi=par->minopti;
   }
   int cnt=0;
   while(addstep(0)>=0){
     if(rangechf()>=0){
         cnt++;
         Application->ProcessMessages();
         if(stopreq==2){
           addmsg("�����. ������� ������ ���.");
           addmsg(makeparlistbest()+"res="+maxres+ "1/res"+1/maxres);
         }
         while(stopreq==2)   Application->ProcessMessages();
         if(stopreq==1){
           addmsg("���������� �� �������");
           goto endl;
         }
         //res=bodyf();
//         double bodyoffset(double offsa,double bodyf()){
         ast s=makeparlist();
         addlog("\n\n"+s+"\n");
         res=bodyoffset(offset,bodyf,inistep);

         if(first){
            maxres=res;
            savepar();
         }
         else if(res>maxres){
           maxres=res;
           savepar();
         }
         first=false;
//         ast s2=makeparlist();
         s+="offs= "+FloatToStrF(offset,ffFixed,8,0)+ "���="+FloatToStrF(res,ffFixed,8,7)+ "      "+FloatToStrF(1/res,ffFixed,8,1);
//         addmsg(s);
        showcstate(s);
//        extern bool parsaved;
         if(res==maxres){
//          if(parsaved){
           parsaved=false;
           addmsg(s);
           showgraph();
        }
     }
   }
endl:
   addmsg("������� ��������, ������ ���������");
   addmsg(makeparlistbest()+" offs="+lastoffs+" res="+maxres+ "  1/res"+1/maxres);
   loadpar();
   res=bodyf();
   addmsg("������������ �������:");
   extern tvector *BR  ;
   double f1,f2,f3;
   f1=BR->Items[0];
   /*
//   par=(tparam *)params->Items[3];
   par=(tparam *)params->Items[1];
   f2=RIM1->Items[0];
   f3=f1/f2*par->cval;
   addmsg(par->name+"="+FloatToStr(f3));

   f1=SiM1->Items[0];
//   par=(tparam *)params->Items[2];
   par=(tparam *)params->Items[2];
   addmsg(par->name+"="+FloatToStr(f1/f2*par->cval));

   f1=SP500->Items[0];
//   par=(tparam *)params->Items[1];
par=(tparam *)params->Items[0];
   addmsg(par->name+"="+FloatToStr(f1/f2*par->cval));
     */
}
#pragma package(smart_init)
