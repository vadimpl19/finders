//---------------------------------------------------------------------------


#pragma hdrstop
#include "VECTORS.h"
#include "math.h"
#include "csv.h"

void alert(AnsiString s);
void addmsg(const ast& s);
////////////////////////////////////////////////////////////////////////////////
double deviation(TList* vec1,TList* vec2,int maxcnt){
////////////////////////////////////////////////////////////////////////////////
    if(vec1->Count!=vec2->Count)return (-1);
//    int register maxf=vec1->Count;

    double register sum=0;

//    for(int i=0;i<maxf;i++){
      for(int i=0;i<maxcnt;i++){
       double register d1,d2;
       d1=*(double*)vec1->Items[i];
       d2=*(double*)vec2->Items[i];
       d1=(d1-d2)*(d1-d2);
       sum+=d1;
    }
    sum=sum/maxcnt;
    sum=sqrt(sum);
}
////////////////////////////////////////////////////////////////////////////////
double deviation(tvector* vec1,tvector* vec2,int shift,int maxcnt){
////////////////////////////////////////////////////////////////////////////////
//    if(vec1->Count!=vec2->Count)return (-1);
    int register maxf,beginf;
    if(shift<0)beginf=0;
    else {
      beginf=shift;
      if(vec1->Count>(vec2->Count+shift))maxf=vec2->Count+shift;
      else  {
        maxf=vec1->Count-shift;
        if((maxf-beginf)>vec2->Count)maxf=vec2->Count+shift;
      }
    }
    if(maxf>maxcnt)maxf=maxcnt;
    double register sum=0;
    if(maxf==beginf)return -1;

    for(int i=beginf;i<maxf;i++){
       double register d1,d2;
       d1=vec1->Items[i-shift];
       d2=vec2->Items[i];
       d1=(d1-d2)*(d1-d2);
       sum+=d1;
    }
    sum=sum/(maxf-beginf);
    sum=sqrt(sum);
    return sum;
}
////////////////////////////////////////////////////////////////////////////////
double deviationavg(TList* vec1,TList* vec2){
////////////////////////////////////////////////////////////////////////////////
    if(vec1->Count!=vec2->Count)return (-1);
    int register maxf=vec1->Count;

    double register sum=0;

    for(int i=0;i<maxf;i++){
       double register d1,d2;
       d1=*(double *)vec1->Items[i];
       d2=*(double *)vec2->Items[i];
       d1=(d2-d1);
       sum+=d1;
    }
    sum=sum/maxf;
    sum=sqrt(sum);
}
////////////////////////////////////////////////////////////////////////////////
double avg(TList* vec1){
////////////////////////////////////////////////////////////////////////////////
    int register maxf=vec1->Count;

    double register sum=0;

    for(int i=0;i<maxf;i++){
       double register d1,d2;
       d1=*(double *)vec1->Items[i];
       sum+=d1;
    }
    sum=sum/maxf;
    sum=sqrt(sum);
}
////////////////////////////////////////////////////////////////////////////////
double avg2(TList* vec1){
////////////////////////////////////////////////////////////////////////////////
    int register maxf=vec1->Count;

    double register sum=0;

    for(int i=0;i<maxf;i++){
       double register d1,d2;
       d1=*(double *)vec1->Items[i];
       sum+=d1*d1;
    }
    sum=sum/maxf;
    sum=sqrt(sum);
}
////////////////////////////////////////////////////////////////////////////////
int loadvectors(const ast& fname,const ast& format,TList** lists){
////////////////////////////////////////////////////////////////////////////////
char * buf;
ast * sp;
double * dp;
int j;
   int h=FileOpen(fname,fmOpenRead);
   if (h==-1){alert("���� "+fname+" �� ��������");return -1;};
   int   iFileLength = FileSeek(h,0,2);
   FileSeek(h,0,0); //
   buf = new char[iFileLength+1];
   int cnt=0;
   int br = FileRead(h, buf, iFileLength);
      if (br==-1){alert("���� "+fname+" �� ��������");return -2;};
      FileClose(h);
      delim=';';

   int i=0;
   try{
   do{
      cnt++;
      for(j=0;j<format.Length();j++){
        char* ch=format.c_str();
//        switch   (format[j]){
        switch   (ch[j]){
        case 's':
        case 'S':
           sp=new AnsiString();
           getitem(sp,buf,&i,br);
           lists[j]->Add(sp);
        break;
        case 'd':
        case 'D':
           dp=new double;
           *dp=getdouble(buf,&i, br );
           lists[j]->Add(dp);
        break;
        default:
           alert("����������� ������ � ������ �������");
            delete [] buf;
            return (-1);
        }

     }
     }while(i<br);
        addmsg("�������� ����� ���������. ����������"+IntToStr(cnt)+"�����");
        delete [] buf;
        return cnt;


//   }catch(EConvertError&){
   }catch(...){
            alert("�������� ������ �����, �������� ��������");

            delete [] buf;
            return (-1);
        }



}
////////////////////////////////////////////////////////////////////////////////
tvector::tvector(const ast& aname,int size){
////////////////////////////////////////////////////////////////////////////////
  name=aname;
  Count=size;
  Items=new double[size];
}
////////////////////////////////////////////////////////////////////////////////
tvector* performvector(TList *l,int shift){
////////////////////////////////////////////////////////////////////////////////
 if(shift<0)return NULL;
 tvector* vec=new tvector("noname",l->Count-shift);
 int maxf=l->Count;
 for (int i=shift;i<maxf;i++){
   vec->Items[i-shift]=*(double*)l->Items[i];
 }
 return vec;
}

//---------------------------------------------------------------------------

#pragma package(smart_init)
