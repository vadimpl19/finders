//---------------------------------------------------------------------------
#define ast AnsiString
#include <vcl.h>
#pragma hdrstop

#include "mainu.h"
#include "VECTORS.h"
#include "optimiz.h"
#include "logs.h"
#include <math.h>
#include <inifiles.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
int shift=0;
Tmainf *mainf;
tspline spline;
//---------------------------------------------------------------------------
__fastcall Tmainf::Tmainf(TComponent* Owner)
        : TForm(Owner)
{
  inilog("!offsadj.log");
  DecimalSeparator='.';
 spline.setparam(40,80,1);
}
void alert(AnsiString s){
      Application->MessageBox(s.c_str(), "������", MB_OK);
};
 tvector *wtib,*wtia,*wtil;
 tvector* brentb,*brenta,*brentl;
 tvector* rib,*ria,*ril;
 tvector *BR, *RIM1,*SiM1,*SP500;
 tvector *synt;
 tvector *usd;
  double br_rad,br_offs,br_alfa;
 double brc,sic,spc;
//---------------------------------------------------------------------------
//#define vectorcnt   13

//double offset=0;
extern double offset;
TList * vectors[vectorcnt];

double wtibc,wtiac,wtilc;
double brentbc,brentac,brentlc;
double basec;
recountoliavg(double* oilavg){
  double maxf=brentb->Count;
  for(int i=0;i<maxf;i++){
     double d=wtia->Items[i];
     *oilavg++=((wtib->Items[i])*wtibc+
               (wtia->Items[i])*wtiac+
               wtil->Items[i]*wtilc+
               brentb->Items[i]*brentbc+
               brenta->Items[i]*brentac+
               brentl->Items[i]*brentlc)*basec+offset;
  }
}


void __fastcall  recountsynt(int maxcnt){
//  double maxf=BR->Count;
  double d;
  double * dp;
  double *brp;
  double *sip;
  dp=&synt->Items[0];
  brp=&BR->Items[0];
  sip=&SiM1->Items[0];
  for(int i=0;i<maxcnt;i++){
     *dp++=brc*(*brp++)-sic*(*sip++)+offset;
  }
}
//////////////////////////////////////////////////////////////////////////////////////
//������ ������� �������� ����� ������� �� ����� brent  ������� 1 - ��������
void __fastcall  recountsyntusd(int maxcnt){
//maxcnt - ����� ������������� �����
//������������ ���������� ���������� tvector BR - ���� �� �����
                        //brc - ����. ������������������
                        //offset - ��������
//���������� tvector synt - ���������� ������
//
//////////////////////////////////////////////////////////////////////////////////////
//  double maxf=BR->Count;
  double d;
  double * dp;
  double *brp;
  double *sip;
  dp=&synt->Items[0];
  brp=&BR->Items[0];

  for(int i=0;i<maxcnt;i++){
     *dp++=brc*(*brp++)+offset;
  }
}
double bexport;
double broffs;
//////////////////////////////////////////////////////////////////////////////////////
//������ ������� �������� ����� ������� �� ����� brent  ������� 2 - ������� ������� �����������������
void __fastcall  recountsyntusd_nemz(int maxcnt){
//maxcnt - ����� ������������� �����
//������������ ���������� ���������� tvector BR - ���� �� �����
                        //bexport - ��������� ��������� �� �������� ������� �� �������� ��
                        //offset - ��������
                        //broffs -�������� ���� �� �����
//���������� tvector synt - ���������� ������
//
//////////////////////////////////////////////////////////////////////////////////////
//  double maxf=BR->Count;
  double d;
  double * dp;
  double *brp;
  double *sip;
  dp=&synt->Items[0];
  brp=&BR->Items[0];

  for(int i=0;i<maxcnt;i++){
     *dp++=bexport/((*brp++)+broffs)+offset;
  }
}





int __fastcall  chfunsynt(){return 1;};
void showcstate(const ast& s){
   mainf->cstatel->Caption=s;
}
void addmsg(const ast& s){
     mainf->msg->SelAttributes->Color=clBlack;
     mainf->msg->Lines->Add(s);


}
int chfun(){
double d=brentbc+brentac+brentlc+wtibc+wtiac+wtilc;
  if((d>1.2)||(d<0.8))return -1;
  return 1;
}
tvector * oilavg;
int maxcnt_gl;
double simple_synt_usd(){

  recountsyntusd(maxcnt_gl);
  double dev=(deviation_vec(synt,usd,maxcnt_gl));
  return 1/dev;

}

double simple_synt_usd_nemz(){

  recountsyntusd_nemz(maxcnt_gl);
  double dev=(deviation_vec(synt,usd,maxcnt_gl));
  return 1/dev;

}

void __fastcall Tmainf::Button1Click(TObject *Sender)
{  //������� ������� �����������
 for (int i=0;i<vectorcnt;i++){ //���������� ������ ��������
   vectors[i]=new TList();
   vectors[i]->Capacity=40000;
 }

//int res=loadvectors("invect_mini_��18_04.csv","sdddd",vectors);
ast s=ExtractFileDir(Application->ExeName)+"\\ini\\ini.ini";
  TIniFile *ini=new TIniFile(s);
  s=ini->ReadString("common","infile","");
//   commentl->Caption=ini->ReadString("common","comment","");
int res=loadvectors(s,"ssdd",vectors);
   commentl->Caption=ini->ReadString("common","comment","");

  double *riavg;

  BR=performvector(vectors[2],0);
  usd=performvector(vectors[3],0);

  extern double offset,inistep,endoffs;
  offset=ini->ReadFloat("common","offsetstart",0);
  inistep=ini->ReadFloat("common","inistep",10000);
  endoffs=ini->ReadFloat("common","endoffs",0);

/*
  s="brc";
  registerparamd(s,-500,500,ini->ReadFloat(s,"begin",0),
                                   ini->ReadFloat(s,"end",0),
                                   ini->ReadFloat(s,"step",1),
                                   &brc);
*/
s="broffs";
  registerparamd(s,-500,500,ini->ReadFloat(s,"begin",0),
                                   ini->ReadFloat(s,"end",0),
                                   ini->ReadFloat(s,"step",1),
                                   &broffs);
s="bexport";
  registerparamd(s,-500,500,ini->ReadFloat(s,"begin",0),
                                   ini->ReadFloat(s,"end",0),
                                   ini->ReadFloat(s,"step",1),
                                   &bexport);

  int maxf=BR->Count;

  synt= new tvector("synt",maxf);

  maxcnt_gl=maxf;
  for(int i=0;i<maxf;i++){ //����� ����������� ����������� � ��������� �������
  ast s =*(ast *)vectors[0]->Items[i];
  s=s+" "+*(ast *)vectors[1]->Items[i];
//      ast * s=(ast *)vectors[0]->Items[i];
//      Series1->AddXY(i,RIM1->Items[i],*s);
      Series1->AddXY(i,usd->Items[i],s);
  }

  //perebor(&chfunsynt,&simple_synt_usd);
  perebor(&chfunsynt,&simple_synt_usd_nemz);
//   maxcnt_gl=maxf;
//   simple_synt_usd();

}
//---------------------------------------------------------------------------
void __fastcall Tmainf::playbClick(TObject *Sender)
{
if(Caption=="����"){
  Caption="�����";
  stopreq=2;
}else{
  Caption="����";
  stopreq=0;
}
}
//---------------------------------------------------------------------------

void __fastcall Tmainf::stopbClick(TObject *Sender)
{
stopreq=1;
}
//---------------------------------------------------------------------------
void showgraph(){
  mainf->Series2->Clear();
 int maxf=BR->Count;
 double oldgl=maxcnt_gl;
 maxcnt_gl=maxf;
  extern bool needlog;
   needlog=true;
//   simple_synt_usd();
   needlog=false;
  for(int i=0;i<maxf;i++){
//      ast * s=(ast *)vectors[0]->Items[i];

      mainf->Series2->AddXY(i,synt->Items[i],"synt");
    //  Series2->AddXY(i,200000,"synt");

  }
  maxcnt_gl=oldgl;
}
const double cos45=0.707106781;

void tspline::setparam(double arad,double aoffs,double aalfa){
  rad=arad;
  offs=aoffs;
  alfa_gr=aalfa;
  alfa_rad=2*M_PI/360*alfa_gr;
  xc=offs+rad*cos45;
  yc=offs-rad*cos45;
  y2=yc+sin(90*2*M_PI/360-alfa_rad)*rad;
  x2=xc-cos(90*2*M_PI/360-alfa_rad)*rad;
  k=tan(alfa_rad);
  b=y2-k*x2;
  radkv=rad*rad;
};
double tspline::recountpoint(double x){
  if(x<=offs){
     return x;
  }
  if(x<x2){
      double d=x-xc;
      d=sqrt(radkv-d*d);
      if(x<xc)return yc+d;
      else return yc-d;
  }
  return k*x+b;
}

